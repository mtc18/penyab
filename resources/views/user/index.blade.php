@extends('layouts.app')

@section('content')
    <h1>Lista de usuarios</h1>
    <a href="/users/create">Nuevo</a>
    <ul>
    @forelse ($users as $user)
        <li>{{ $user->name }}: {{ $user->email }}
            <a href="/users/{{ $user->id }}/edit">Editar</a>

            <form method="post" action="/users/{{ $user->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                <input type="submit" value="borrar">
            </form>
        </li>
    @empty
        <li>No hay usuarios!!</li>
    @endforelse
    </ul>

    {{ $users->render() }}

@endsection
