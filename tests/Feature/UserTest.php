<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;

class UserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    use RefreshDatabase;

    /**
    * @group users
    * @group users1
    */
    public function test_lista_de_usuarios_vacia()
    {
      $response = $this->get('/users');
      $response->assertStatus(200);
      $response->assertSee('Lista de usuarios');
      $response->assertSee('No hay usuarios');
    }
    public function test_lista_de_usuarios()
    {
      factory(User::class)->create([
        'name' => 'Pepe',
        'email' => 'pepe@gmail.com'
      ]);

      factory(User::class, 100)->create();

      $response = $this->get('/users');
      $response->assertStatus(200);
      $response->assertDontSee('No hay usuarios');
      $response->assertSee('Lista de usuarios');
      $response->assertSee('Pepe');
      $response->assertSee('pepe@gmail.com');
    }

    public function test_metodo_show_user_1()
    {
      factory(User::class)->create([
        'id' => 1,
        'name' => 'Pepe',
        'email' => 'pepe@gmail.com'
      ]);

      $response = $this->get('/users/1');
      $response->assertStatus(200);
      $response->assertSee('detalle del usuario 1');
      $response->assertSee('Pepe');
      $response->assertSee('pepe@gmail.com');
    }
    public function test_metodo_show_user_inexistente()
    {
      $response = $this->get('/users/100000');
      $response->assertStatus(404);
    }


    public function test_store_user()
    {
        $this->post('/users', [
            'name' => 'Pepe',
            'email' => 'pepe@gmail.com',
            'password' => 'secret'
        ])->assertRedirect('users');

        //tabla normal
        $this->assertDatabaseHas('users', [
            'name' => 'Pepe',
            'email' => 'pepe@gmail.com',
            // 'password' => 'secret'
        ]);
        //tabla de usuarios: comprueba campos y contraseña cifrada
        $this->assertCredentials([
            'name' => 'Pepe',
            'email' => 'pepe@gmail.com',
            'password' => 'secret'
        ]);
    }


  }
